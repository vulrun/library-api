"use strict";
require("dotenv").config();

const express = require("express");
const app = express();
const http = require("http");
const logger = require("morgan");
const responses = require("./src/responses");
const connection = require("./src/connection");

// middlewares
app.set("trust proxy", true);
app.set("port", process.env.PORT);
app.use(responses());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use("/", require("./src/routes"));

// handling errors
app.use((req, res, next) => next("404, Not Found"));
app.use((err, req, res, next) => {
  console.error(err);
  return res.status(err?.status || 400).send({ error: err?.message || err || "ERROR_AHEAD" });
});

module.export = http.createServer(app).listen(app.get("port"), () => {
  console.log("Listening on", app.get("port"));
  connection.mongodb();
});
