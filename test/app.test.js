require("../app");
const app = "127.0.0.1:3000";
const connection = require("../src/connection");
const chai = require("chai");
const chaiHttp = require("chai-http");
chai.use(chaiHttp);
const { expect } = chai;

describe("Library Application API", () => {
  describe("GET /books", () => {
    it("should return a list of books", async () => {
      const res = await chai.request(app).get("/books");

      expect(res).to.have.status(200);
      expect(res.body).to.have.property("message").equal("fetched successfully");
      expect(res.body).to.have.property("response").to.be.an("array");
    });
  });

  describe("POST /users/:userId/books", () => {
    it("should add a book to the user's collection", async () => {
      const userId = "648c085f34b9a2450f2a8a7a";
      const bookData = {
        title: "Sample Book",
        description: "This is a sample book.",
        publication_year: 2021,
        author_ids: ["648c0bb834b9a2450f2ca2b5"],
        genre_ids: ["648c0bb834b9a2450f2ca2b5"],
      };

      const res = await chai.request(app).post(`/users/${userId}/books`).send(bookData);

      expect(res).to.have.status(200);
      expect(res.body).to.have.property("message").equal("book added successfully");
    });

    it("should return an error for invalid book data", async () => {
      const userId = "648c085f34b9a2450f2a8a7a";
      const bookData = {
        // Missing required fields
      };

      const res = await chai.request(app).post(`/users/${userId}/books`).send(bookData);

      expect(res).to.have.status(400);
      expect(res.body).to.have.property("error");
    });

    it("should return an error for non-existing user", async () => {
      const userId = "nonExistingUser";
      const bookData = {
        title: "Sample Book",
        description: "This is a sample book.",
        publication_year: 2021,
        author_ids: ["648c0bb834b9a2450f2ca2b5"],
        genre_ids: ["648c0bb834b9a2450f2ca2b5"],
      };

      const res = await chai.request(app).post(`/users/${userId}/books`).send(bookData);

      expect(res).to.have.status(400);
      expect(res.body).to.have.property("error");
    });
  });

  describe("DELETE /users/:userId/books/:bookId", () => {
    it("should remove a book from the user's collection", async () => {
      const userId = "648c085f34b9a2450f2a8a7a";
      const bookId = "648c1466a596a8b6e3b67cea";

      const res = await chai.request(app).delete(`/users/${userId}/books/${bookId}`);

      expect(res).to.have.status(200);
      expect(res.body).to.have.property("message").equal("book removed successfully");
    });

    it("should return an error for non-existing user", async () => {
      const userId = "nonExistingUser";
      const bookId = "648c0c7bcc7017429193c167";

      const res = await chai.request(app).delete(`/users/${userId}/books/${bookId}`);

      expect(res).to.have.status(400);
      expect(res.body).to.have.property("error");
    });

    it("should return an error for non-existing book", async () => {
      const userId = "648c085f34b9a2450f2a8a7a";
      const bookId = "nonExistingBook";

      const res = await chai.request(app).delete(`/users/${userId}/books/${bookId}`);

      expect(res).to.have.status(400);
      expect(res.body).to.have.property("error");
    });
  });

  describe("PUT /reviews/:reviewId", () => {
    it("should update a review for a book", async () => {
      const reviewId = "648c0d2134b9a2450f2d81a2";
      const reviewData = {
        rating: 5,
        comment: "This book is excellent!",
      };

      const res = await chai.request(app).put(`/reviews/${reviewId}`).send(reviewData);

      expect(res).to.have.status(200);
      expect(res.body).to.have.property("message").equal("review updated successfully");
    });

    it("should return an error for invalid review data", async () => {
      const reviewId = "648c0d2134b9a2450f2d81a2"; // Provide a valid review ID
      const reviewData = {
        // Missing required fields
      };

      const res = await chai.request(app).put(`/reviews/${reviewId}`).send(reviewData);

      expect(res).to.have.status(400);
      expect(res.body).to.have.property("error");
    });

    it("should return an error for non-existing review", async () => {
      const reviewId = "nonExistingReview";
      const reviewData = {
        rating: 5,
        comment: "This book is excellent!",
      };

      const res = await chai.request(app).put(`/reviews/${reviewId}`).send(reviewData);

      expect(res).to.have.status(400);
      expect(res.body).to.have.property("error");
    });
  });

  describe("GET /search", () => {
    it("should return a list of books matching the search query", async () => {
      const searchQuery = "Sample"; // Provide a valid search query

      const res = await chai.request(app).get(`/search?q=${searchQuery}`);

      expect(res).to.have.status(200);
      // expect(res.body).to.be.an("array");
    });
  });
});
