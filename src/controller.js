const async = require("async");
const Model = require("./model");

module.exports = {
  getBooks,
  addBook,
  removeBook,
  updateReview,
  searchBook,
};

// Fetch the list of books from the database
async function getBooks(req, res, next) {
  try {
    const books = await Model.Books.find({}).lean();

    res.success("fetched successfully", books);
  } catch (err) {
    next(err);
  }
}

// Add a book to the user's collection
function addBook(req, res, next) {
  const userId = req.params.userId;
  const bookData = req.body;

  async.waterfall(
    [
      // Check if the user exists
      (callback) => {
        Model.Users.findOne({ _id: userId }, (err, user) => {
          if (err) {
            callback("failed to check user existence");
          } else if (!user) {
            callback("user not found");
          } else {
            callback(null);
          }
        });
      },
      // Add the book to the user's collection
      (callback) => {
        Model.Books.create(bookData, (err, result) => {
          if (err) {
            console.log(err);
            callback("failed to add book to collection");
          } else {
            callback(null, result.insertedid);
          }
        });
      },
      // Update the user's collection with the new book
      (bookId, callback) => {
        Model.Users.updateOne({ _id: userId }, { $push: { book_ids: bookId } }, (err) => {
          if (err) {
            callback("failed to update user collection");
          } else {
            callback(null, bookId);
          }
        });
      },
    ],
    (err, bookId) => {
      if (err) {
        next(err);
      } else {
        res.success("book added successfully", bookId);
      }
    }
  );
}

// Remove a book from the user's collection
function removeBook(req, res, next) {
  const userId = req.params.userId;
  const bookId = req.params.bookId;

  async.waterfall(
    [
      // Check if the user exists
      (callback) => {
        Model.Users.findOne({ _id: userId }, (err, user) => {
          if (err) {
            callback("failed to check user existence");
          } else if (!user) {
            callback("user not found");
          } else {
            callback(null);
          }
        });
      },
      // Remove the book from the user's collection
      (callback) => {
        Model.Books.deleteOne({ _id: bookId }, (err, result) => {
          if (err) {
            callback("failed to remove book from collection");
          } else if (result.deletedCount === 0) {
            callback("book not found in the collection");
          } else {
            callback(null);
          }
        });
      },
      // Update the user's collection by removing the book reference
      (callback) => {
        Model.Users.updateOne({ _id: userId }, { $pull: { book_ids: bookId } }, (err) => {
          if (err) {
            callback("failed to update user collection");
          } else {
            callback(null);
          }
        });
      },
    ],
    (err) => {
      if (err) {
        next(new Error(err));
      } else {
        res.success("book removed successfully", bookId);
      }
    }
  );
}

// Update a review for a book
async function updateReview(req, res, next) {
  try {
    const { reviewId } = req.params;
    const reviewData = req.body;
    if (!Object.keys(reviewData).length) throw new Error("no review");

    const result = await Model.Reviews.updateOne({ _id: reviewId }, { $set: reviewData }).lean();
    if (!result) throw new Error("no review");

    return res.success("review updated successfully", result);
  } catch (err) {
    next(err);
  }
}

// Search for books by title, author, or genre
async function searchBook(req, res, next) {
  try {
    const searchQuery = req.query.q;
    const searchObjIds = [].concat(searchQuery);

    const ors = [
      {
        title: {
          $regex: searchQuery,
          $options: "i",
        },
      },
    ];

    if (searchObjIds.length && ObjectId.isValid(searchQuery)) {
      ors.push({ author_ids: { $in: searchObjIds } });
      ors.push({ genre_ids: { $in: searchObjIds } });
    }

    const books = await Model.Books.find({ $or: ors }).lean();

    return res.success("fetched successfully", books);
  } catch (err) {
    next(err);
  }
}
