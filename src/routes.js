const express = require("express");
const router = express.Router();
const validator = require("./validator");
const controller = require("./controller");

// Fetch the list of books
router.get("/books", controller.getBooks);

// Add a book
router.post("/users/:userId/books", validator.validateBook, controller.addBook);

// Remove a book
router.delete("/users/:userId/books/:bookId", controller.removeBook);

// Update a review for a book
router.put("/reviews/:reviewId", controller.updateReview);

// Search for books
router.get("/search", controller.searchBook);

module.exports = router;
