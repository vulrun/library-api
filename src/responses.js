// Express Responses
module.exports = () => (req, res, next) => {
  // success response
  res.success = (message, response) => {
    message = message || "";
    response = response || {};

    return res.status(200).json({
      status: true,
      message: message,
      response: response,
    });
  };

  // move forward
  next();
};
