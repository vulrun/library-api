const mongoose = require("mongoose");

const UserSchema = new mongoose.Schema(
  {
    name: { type: String, default: "" },
    email: { type: String, default: "", index: true },
    password: { type: String, default: "" },
    address: { type: String, default: "" },
  },
  {
    timestamps: true,
  }
);

const BookSchema = new mongoose.Schema(
  {
    title: { type: String, default: "" },
    description: { type: String, default: "" },
    publication_year: { type: Number, default: 1970 },
    genre_ids: [{ type: ObjectId, ref: "genres" }],
    author_ids: [{ type: ObjectId, ref: "authors" }],
  },
  {
    timestamps: true,
  }
);

const GenreSchema = new mongoose.Schema(
  {
    name: { type: String, default: "" },
    book_ids: [{ type: ObjectId, ref: "books" }],
  },
  {
    timestamps: true,
  }
);

const AuthorSchema = new mongoose.Schema(
  {
    name: { type: String, default: "" },
    bio: { type: String, default: "" },
    book_ids: [{ type: ObjectId, ref: "books" }],
  },
  {
    timestamps: true,
  }
);

const ReviewSchema = new mongoose.Schema(
  {
    user_id: { type: ObjectId, ref: "users" },
    book_id: { type: ObjectId, ref: "books" },
    rating: { type: Number, default: 0 },
    comment: { type: String, default: "" },
  },
  {
    timestamps: true,
  }
);

module.exports = {
  Users: mongoose.model("users", UserSchema),
  Books: mongoose.model("books", BookSchema),
  Genres: mongoose.model("genres", GenreSchema),
  Authors: mongoose.model("authors", AuthorSchema),
  Reviews: mongoose.model("reviews", ReviewSchema),
};
