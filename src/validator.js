const Joi = require("joi");

function validateBook(req, res, next) {
  const bookSchema = Joi.object({
    title: Joi.string().required(),
    description: Joi.string().required(),
    publication_year: Joi.number().integer().required(),
    author_ids: Joi.array().items(Joi.string().required()).required(),
    genre_ids: Joi.array().items(Joi.string().required()).required(),
  });

  const { error } = bookSchema.validate(req.body);
  if (error) {
    next(error.details[0].message);
  } else {
    next();
  }
  return;
}

// Validate input for updating a review
function validateReview(req, res, next) {
  const reviewSchema = Joi.object({
    book_id: Joi.string().required(),
    user_id: Joi.string().required(),
    rating: Joi.number().integer().min(1).max(5).required(),
    comment: Joi.string().required(),
  });

  const { error } = reviewSchema.validate(req.body);
  if (error) {
    next(error.details[0].message);
  } else {
    next();
  }
  return;
}

module.exports = {
  validateBook,
  validateReview,
};
